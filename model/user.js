const mongoose = require('mongoose');


const Schema = mongoose.Schema;

const UserSchema = new Schema({
  first_name: {
      type: String,
      required: true
  },
  last_name: {
    type: String,
    required: true
  },
  role: {
    type: String,
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  token: { type: String },
});


module.exports = mongoose.model('user', UserSchema);

